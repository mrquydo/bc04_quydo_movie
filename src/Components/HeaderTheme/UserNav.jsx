import { render } from '@testing-library/react';
import React from 'react'
import { useSelector } from 'react-redux'
import { NavLink } from 'react-router-dom'
import { localServ } from '../../services/localServices';


export default function UserNav() {
    let user = useSelector((state) => { return state.userReducer.userInfor; });
    console.log('user: ', user);

    let handleLogout = () => {
        // Xoa data tu local storage
        localServ.user.remove();
        // remove data ru redux neu khong reload lai trang
        // dispatch({type: SET_USER payload: null})

        window.location.href = "/login"
    }

    let renderContent = () => {
        if (user) {
            return (
                <>
                    <span className='font-medium text-blue-800 underline'>{user.hoTen}</span>
                    <button onClick={handleLogout} className='border rounded border-red-500 px-5 py-2 text-red-500'>Dang Xuat</button>
                </>
            )
        } else {
            return (
                <>
                    <NavLink to="/login"><button className='border rounded border-black px-5 py-2 text-black hover:text-white hover:bg-black transition'>Dang nhap</button></NavLink>
                    <button className='border rounded border-red-500 px-5 py-2 text-red-500'>Dang Ky</button>
                </>
            )
        }
    }
    return (
        <div className='space-x-5'>
            {renderContent()}
        </div>
    )
}
