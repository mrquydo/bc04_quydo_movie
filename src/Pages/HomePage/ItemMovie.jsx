import React from 'react'
import { Card } from 'antd';
import { NavLink } from 'react-router-dom';
const { Meta } = Card;

export default function ItemMovie({ data }) {
    console.log('data: ', data);
    return (

        <Card
            hoverable
            style={{ width: "100%" }}
            cover={<img className='h-80 w-full object-cover' alt="example" src={data.hinhAnh} />}
        >
            <Meta title={<p className='text-red-500 truncate'>{data.tenPhim}</p>} description="www.instagram.com" />
            <NavLink to={`/detail/${data.maPhim}`}><button className='w-full py-2 text-center bg-red-500 text-white mt-5 rounded hover:bg-black transition cursor-pointer'>Xem chi tiết</button></NavLink>
        </Card>

    )
}
