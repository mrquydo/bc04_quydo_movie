import { localServ } from "../../services/localServices";
import { userService } from "../../services/userService";
import { SET_USER } from "../constants/constantUser";

const setUserLoginSuccess = (successValue) => {
    return {
        type: SET_USER,
        payload: successValue
    }
}


export const setUserLoginActionServ = (dataLogin, onLoginSuccess, onLoginFail) => {
    return (dispatch) => {
        userService.postLogin(dataLogin)
            .then((res) => {
                console.log(res);
                localServ.user.set(res.data.content)
                onLoginSuccess()
                // dispatch({
                //     type: SET_USER,
                //     payload: res.data.content
                // })
                dispatch(setUserLoginSuccess(res.data.content))
            })
            .catch((err) => {
                onLoginFail()
                console.log(err);
            });
    }
}